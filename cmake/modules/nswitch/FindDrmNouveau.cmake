include(FindPackageHandleStandardArgs)

if(NOT SWITCH)
    message(FATAL_ERROR "This helper can only be used when cross-compiling for the Switch.")
endif()

set(DRM_NOUVEAU_PATHS ${PORTLIBS})
find_path(DRM_NOUVEAU_INCLUDE_DIR nouveau_drm.h
        PATHS ${DRM_NOUVEAU_PATHS}
        PATH_SUFFIXES include)

find_library(DRM_NOUVEAU_LIBRARY NAMES libdrm_nouveau.a
        PATHS ${DRM_NOUVEAU_PATHS}
        PATH_SUFFIXES lib)

set(DRM_NOUVEAU_INCLUDE_DIRS ${DRM_NOUVEAU_INCLUDE_DIR})
set(DRM_NOUVEAU_LIBRARIES ${DRM_NOUVEAU_LIBRARY})

find_package_handle_standard_args(DrmNouveau DEFAULT_MSG
        DRM_NOUVEAU_INCLUDE_DIR DRM_NOUVEAU_LIBRARY)

mark_as_advanced(DRM_NOUVEAU_INCLUDE_DIR DRM_NOUVEAU_LIBRARY)

if(DrmNouveau_FOUND)
    set(DrmNouveau ${DRM_NOUVEAU_INCLUDE_DIR}/../..)

    add_library(switch::DrmNouveau STATIC IMPORTED GLOBAL)
    target_link_libraries(switch::DrmNouveau INTERFACE switch::libnx)
    set_target_properties(switch::DrmNouveau PROPERTIES
            IMPORTED_LOCATION "${DRM_NOUVEAU_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${DRM_NOUVEAU_INCLUDE_DIR}")
endif()