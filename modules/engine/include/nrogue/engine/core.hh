#pragma once

namespace nrogue
{
    void InitializeEngine();
    void ExitEngine();

    void EarlyPanic();
}