include(FindPackageHandleStandardArgs)

if(NOT SWITCH)
    message(FATAL_ERROR "This helper can only be used when cross-compiling for the Switch.")
endif()

set(GLAPI_PATHS ${PORTLIBS})
find_path(GLAPI_INCLUDE_DIR GL/gl.h
        PATHS ${GLAPI_PATHS}
        PATH_SUFFIXES include)

find_library(GLAPI_LIBRARY NAMES libglapi.a
        PATHS ${GLAPI_PATHS}
        PATH_SUFFIXES lib)

set(GLAPI_INCLUDE_DIRS ${GLAPI_INCLUDE_DIR})
set(GLAPI_LIBRARIES ${GLAPI_LIBRARY})

find_package_handle_standard_args(GLApi DEFAULT_MSG
        GLAPI_INCLUDE_DIR GLAPI_LIBRARY)

mark_as_advanced(GLAPI_INCLUDE_DIR GLAPI_LIBRARY)

if(GLAPI_FOUND)
    set(GLApi ${GLAPI_INCLUDE_DIR}/../..)

    add_library(switch::GLApi STATIC IMPORTED GLOBAL)
    set_target_properties(switch::GLApi PROPERTIES
            IMPORTED_LOCATION "${GLAPI_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${GLAPI_INCLUDE_DIR}")
endif()