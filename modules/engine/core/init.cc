#include "nrogue/engine/core.hh"

#include <iostream>
#include "nrogue/engine/service_sys.hh"
#include "nrogue/engine/logging.hh"

namespace nrogue
{
    void InitializeEngine()
    {
        gServiceCollection.RegisterService("ILoggerFactory", new impl::LoggerFactoryImpl());
    }

    void ExitEngine()
    {
        EarlyPanic();
    }
}