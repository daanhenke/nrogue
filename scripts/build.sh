#!/usr/bin/env sh

#TARGET_ARCH="x86_64"
#TARGET_OS="unix"
TARGET_ID="$TARGET_ARCH-$TARGET_OS"

echo "Building for $TARGET_ID"

REPO_DIR="."
BUILD_DIR="$REPO_DIR/build/$TARGET_ID"

[ -d "$BUILD_DIR" ] || mkdir -p "$BUILD_DIR"

if [ "$TARGET_ID" = "x86_64-unix" ]; then
  CC=clang CXX=clang++ cmake \
    -DNROGUE_ARCH="linux" \
    -B "$BUILD_DIR" "$REPO_DIR"
elif [ "$TARGET_ID" = "x86_64-windows" ]; then
  x86_64-w64-mingw32-cmake \
    -DNROGUE_ARCH="windows" \
    -B "$BUILD_DIR" "$REPO_DIR"
elif [ "$TARGET_ID" = "aarch64-nswitch" ]; then
  export DEVKITPRO="/opt/devkitpro"
  export DEVKITA64="$DEVKITPRO/devkitA64"
  cmake \
    -DCMAKE_TOOLCHAIN_FILE="$REPO_DIR/cmake/toolchain/DevkitA64Libnx.cmake" \
    -DNROGUE_ARCH="nswitch" \
    -B "$BUILD_DIR" "$REPO_DIR"
else
  echo "Invalid target"
fi

cmake --build "$BUILD_DIR"