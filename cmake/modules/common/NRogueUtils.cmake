function(add_nrogue_module NAME)
    string(REPLACE "::" "__" NAME_UGLY ${NAME})

    add_library(${NAME_UGLY} INTERFACE)
    if (ARGN)
        message(STATUS "Sources for '${NAME}: ${ARGN}'")
        target_sources(${NAME_UGLY} PUBLIC ${ARGN})
    endif()
    add_library(${NAME} ALIAS ${NAME_UGLY})

    target_include_directories(${NAME_UGLY} INTERFACE "${CMAKE_CURRENT_SOURCE_DIR}/include")
endfunction()

function(add__combined_nrogue_module NAME)
    string(REPLACE "::" "__" NAME_UGLY ${NAME})
    add_nrogue_module(${NAME})

    target_link_libraries(${NAME_UGLY} INTERFACE ${ARGN})
endfunction()

function(print_config)
    message(STATUS "------------------------")
    message(STATUS "Architecture: ${NROGUE_ARCHITECTURE}")
    message(STATUS "------------------------")
endfunction()