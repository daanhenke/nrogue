#include "nrogue/engine/logging.hh"

namespace nrogue::impl
{
    ILogger* LoggerFactoryImpl::CreateLogger(const std::string &name)
    {
        return new LoggerImpl(name);
    }
}