docker run \
  --volume "$PWD:/repo" \
  --workdir "/repo" \
  --env "TARGET_ARCH=$TARGET_ARCH" \
  --env "TARGET_OS=$TARGET_OS" \
  buildbot:latest \
  sh -c "sh ./scripts/build.sh && sh ./scripts/package.sh"

#  registry.gitlab.com/daanhenke/nrogue/buildbot:latest \
