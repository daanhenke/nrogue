## A CMake helper to find libtwili on the system.
##
## On success, it will define:
## > LIBTWILI_FOUND        - The system has libtwili
## > LIBTWILI_INCLUDE_DIRS - The libtwili include directories
## > LIBTWILI_LIBRARIES    - The static libtwili libraries
##
## It also adds an imported target named `switch::libtwili`.
##
## ```
## target_link_libraries(my_executable switch::libtwili)
## ```
## is equivalent to
## ```
## target_include_directories(my_executable PRIVATE ${LIBTWILI_INCLUDE_DIRS})
## target_link_libraries(my_executable ${LIBTWILI_LIBRARIES})
## ```

include(FindPackageHandleStandardArgs)

if(NOT SWITCH)
    message(FATAL_ERROR "This helper can only be used when cross-compiling for the Switch.")
endif()

set(EGL_PATHS ${PORTLIBS})
find_path(EGL_INCLUDE_DIR EGL/egl.h
        PATHS ${EGL_PATHS}
        PATH_SUFFIXES include)

find_library(EGL_LIBRARY NAMES libEGL.a
        PATHS ${EGL_PATHS}
        PATH_SUFFIXES lib)

set(EGL_INCLUDE_DIRS ${EGL_INCLUDE_DIR})
set(EGL_LIBRARIES ${EGL_LIBRARY})

find_package_handle_standard_args(EGL DEFAULT_MSG
        EGL_INCLUDE_DIR EGL_LIBRARY)

mark_as_advanced(EGL_INCLUDE_DIR EGL_LIBRARY)

if(EGL_FOUND)
    set(EGL ${EGL_INCLUDE_DIR}/../..)

    add_library(switch::EGL STATIC IMPORTED GLOBAL)
    set_target_properties(switch::EGL PROPERTIES
            IMPORTED_LOCATION "${EGL_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${EGL_INCLUDE_DIR}")
endif()