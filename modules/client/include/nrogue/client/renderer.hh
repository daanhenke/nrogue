#pragma once

namespace nrogue::client
{
    class IRenderer
    {
    public:
        virtual void StartFrame();
        virtual void EndFrame();
    };
}