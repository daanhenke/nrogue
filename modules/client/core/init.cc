#include <nrogue/engine/core.hh>
#include <iostream>
#include <SDL2/SDL.h>

namespace nrogue
{
    const SDL_Rect fsRect = { 0, 0, 1280, 720 };

    void InitializeClient()
    {
        std::cout << "Initializing client" << std::endl;
        if (SDL_Init(SDL_INIT_VIDEO) < 0)
        {
            std::cout << "SDL2 Init failed: " << SDL_GetError() << std::endl;
            return;
        }

        SDL_InitSubSystem(SDL_INIT_JOYSTICK);
        SDL_JoystickEventState(SDL_ENABLE);
        SDL_JoystickOpen(0);

        auto window = SDL_CreateWindow(
            "Test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            1280, 720,
            SDL_WINDOW_SHOWN
        );
        if (window == nullptr)
        {
            std::cout << "SDL2 CreateWindow failed: " << SDL_GetError() << std::endl;
            return;
        }

        auto renderer = SDL_CreateRenderer(window, 0, SDL_RendererFlags::SDL_RENDERER_ACCELERATED);
        if (renderer == nullptr)
        {
            std::cout << "SDL2 CreateRenderer failed: " << SDL_GetError() << std::endl;
            return;
        }

        auto surface = SDL_GetWindowSurface(window);
        SDL_Event event;
        auto running = true;
        while (running)
        {
            while (SDL_PollEvent(&event))
            {
                switch (event.type)
                {
                    case SDL_QUIT:
                    case SDL_CONTROLLERBUTTONUP:
                    case SDL_JOYBUTTONUP:
                    case SDL_KEYUP:
                        running = false;
                        break;
                }
            }

            SDL_RenderClear(renderer);
            SDL_SetRenderDrawColor(renderer, 0xff, 0x00, 0xff, 0xff);
            SDL_RenderFillRect(renderer, &fsRect);

            SDL_RenderPresent(renderer);
        }
    }

    void ExitClient()
    {

    }
}