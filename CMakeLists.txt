cmake_minimum_required(VERSION 3.24)
project(nrogue)

list(APPEND CMAKE_MODULE_PATH
    "${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules/common"
)

if(SWITCH)
    list(APPEND CMAKE_MODULE_PATH
        "${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules/nswitch"
    )

endif()

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)

set(NROGUE_ARCH "linux" CACHE STRING "Target architecture")

include(NRogueUtils)
print_config()

add_subdirectory(modules/engine)
add_subdirectory(modules/game)
add_subdirectory(modules/client)
