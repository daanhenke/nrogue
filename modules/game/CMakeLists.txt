find_package(SDL2 REQUIRED)

add_executable(nrogue
        game.cc
)

target_link_libraries(nrogue nrogue::engine nrogue::client SDL2::SDL2)

if(SWITCH)
    include(SwitchTools)
    find_package(Libnx REQUIRED)
    find_package(EGL REQUIRED)
    find_package(GLApi REQUIRED)
    find_package(DrmNouveau REQUIRED)

    set_target_properties(nrogue PROPERTIES
        APP_TITLE "nrogue"
        APP_AUTHOR "Daan Henke"
        APP_VERSION "0.0.1"
    )

    target_link_libraries(nrogue switch::libnx switch::EGL switch::GLApi switch::DrmNouveau)
    add_nro_target(nrogue)
endif()