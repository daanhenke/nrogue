#include "nrogue/engine/logging.hh"
#include <utility>
#include <iostream>

namespace nrogue::impl
{
    LoggerImpl::LoggerImpl(std::string name) : _name(std::move(name)) {}

    void LoggerImpl::Raw(std::string fmt, ...)
    {
        std::cout << fmt << std::endl;
    }
}