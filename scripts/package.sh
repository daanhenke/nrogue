#!/usr/bin/env sh

BUILD_DIR="build"
OUT_DIR="build/out"

copy_if_exists()
{
  SOURCE_PATH="$1"
  [ ! -f "$SOURCE_PATH" ] && return

  DEST_PATH="$OUT_DIR/$2"
  echo "Copying $1 > $2"
  DEST_DIR=$(dirname "$DEST_PATH")
  [ -d "$DEST_DIR" ] || mkdir -p "$DEST_DIR"
  cp "$SOURCE_PATH" "$DEST_PATH"
}

# Unix
copy_if_exists "$BUILD_DIR/x86_64-unix/modules/game/nrogue" "x86_64-unix/nrogue"

# Windows
copy_if_exists "$BUILD_DIR/x86_64-windows/modules/game/nrogue.exe" "x86_64-windows/nrogue.exe"
copy_if_exists "/usr/x86_64-w64-mingw32/bin/libstdc++-6.dll" "x86_64-windows/libstdc++-6.dll"
copy_if_exists "/usr/x86_64-w64-mingw32/bin/SDL2.dll" "x86_64-windows/SDL2.dll"
7z a "$OUT_DIR/x86_64-windows.zip" $OUT_DIR/x86_64-windows/*

# NSwitch
copy_if_exists "$BUILD_DIR/aarch64-nswitch/modules/game/nrogue.nro" "aarch64-nswitch/nrogue.nro"