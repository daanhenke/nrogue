#include "nrogue/engine/service_sys.hh"

namespace nrogue
{
    void ServiceCollection::RegisterServiceRaw(std::string name, void *impl)
    {
        auto hash = _hasher(name);
        assert(! _collection.contains(hash));
        _collection[hash] = impl;
    }

    void *ServiceCollection::GetServiceRaw(std::string name)
    {
        auto hash = _hasher(name);

        if (_collection.contains(hash))
        {
            return _collection[hash];
        }

        return nullptr;
    }

    ServiceCollection gServiceCollection;
}