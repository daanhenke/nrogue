#pragma once

#include <map>
#include <string>
#include <cassert>
#include <utility>

namespace nrogue
{
    class ServiceCollection
    {
    private:
        std::map<size_t , void*> _collection;
        std::hash<std::string> _hasher;

    public:
        ServiceCollection() = default;

        void* GetServiceRaw(std::string name);
        template <typename T>
        T* GetService(std::string name) { return static_cast<T*>(GetServiceRaw(std::move(name))); }

        void RegisterServiceRaw(std::string name, void* impl);
        template <typename T>
        void RegisterService(std::string name, T* impl) { RegisterServiceRaw(std::move(name), static_cast<void*>(impl)); }
    };

    extern ServiceCollection gServiceCollection;
}
