#include <nrogue/engine/core.hh>
#include <nrogue/engine/service_sys.hh>
#include <nrogue/engine/logging.hh>
#include <nrogue/client/core.hh>

int main(int argc, const char** argv)
{
    using namespace nrogue;

    InitializeEngine();
    InitializeClient();

    auto loggerFactory = gServiceCollection.GetService<ILoggerFactory>("ILoggerFactory");
    auto logger = loggerFactory->CreateLogger("Test");
    logger->Raw("Test");

    ExitClient();
    ExitEngine();
    return 0;
}

void nrogue::EarlyPanic()
{

}