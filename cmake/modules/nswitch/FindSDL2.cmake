## A CMake helper to find libtwili on the system.
##
## On success, it will define:
## > LIBTWILI_FOUND        - The system has libtwili
## > LIBTWILI_INCLUDE_DIRS - The libtwili include directories
## > LIBTWILI_LIBRARIES    - The static libtwili libraries
##
## It also adds an imported target named `switch::libtwili`.
##
## ```
## target_link_libraries(my_executable switch::libtwili)
## ```
## is equivalent to
## ```
## target_include_directories(my_executable PRIVATE ${LIBTWILI_INCLUDE_DIRS})
## target_link_libraries(my_executable ${LIBTWILI_LIBRARIES})
## ```

include(FindPackageHandleStandardArgs)

if(NOT SWITCH)
    message(FATAL_ERROR "This helper can only be used when cross-compiling for the Switch.")
endif()

set(SDL2_PATHS ${SDL2} $ENV{SDL2} ${PORTLIBS})
find_path(SDL2_INCLUDE_DIR SDL2/SDL.h
        PATHS ${SDL2_PATHS}
        PATH_SUFFIXES include)
message(STATUS ${SDL2_INCLUDE_DIR})

find_library(SDL2_LIBRARY NAMES libSDL2.a
        PATHS ${SDL2_PATHS}
        PATH_SUFFIXES lib)

set(SDL2_INCLUDE_DIRS ${SDL2_INCLUDE_DIR})
set(SDL2_LIBRARIES ${SDL2_LIBRARY})

find_package_handle_standard_args(SDL2 DEFAULT_MSG
        SDL2_INCLUDE_DIR SDL2_LIBRARY)

mark_as_advanced(SDL2_INCLUDE_DIR SDL2_LIBRARY)

if(SDL2_FOUND)
    set(SDL2 ${SDL2_INCLUDE_DIR}/../..)

    add_library(SDL2::SDL2 STATIC IMPORTED GLOBAL)
    set_target_properties(SDL2::SDL2 PROPERTIES
            IMPORTED_LOCATION "${SDL2_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${SDL2_INCLUDE_DIR}")
endif()