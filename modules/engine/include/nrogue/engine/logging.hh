#pragma once

#include <string>

namespace nrogue
{
    class ILogger
    {
    public:
        virtual void Raw(std::string fmt, ...) = 0;
    };

    class ILogBackend
    {
    public:
        virtual void HandleMessage() = 0;
    };

    class ILoggerFactory
    {
    public:
        virtual ILogger* CreateLogger(const std::string& name) = 0;
    };

    namespace impl
    {
        class LoggerFactoryImpl : public ILoggerFactory
        {
        public:
            ILogger* CreateLogger(const std::string &name) override;
        };

        class LoggerImpl : public ILogger
        {
        public:
            LoggerImpl(std::string name);
            void Raw(std::string fmt, ...) override;

        private:
            std::string _name;
        };
    }
}